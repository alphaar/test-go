package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/fravega/test-go/internal/app/handlers"
	"github.com/fravega/test-go/internal/app/middlewares"
)

func main() {

	r := mux.NewRouter()

	// It need the end point to avoid errors
	r.
		HandleFunc("/books/{title}/page/{page}", middlewares.Logger(handlers.BookHandler(), "book")).
		Name("books-index").
		Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", r))
}
