FROM golang:1.9 as build-env

ARG APP_VERSION=dev
ARG GOOS=linux
ARG GOARCH=amd64
ARG CGO_ENABLED=0

WORKDIR /go/src/github.com/fravega/test-go

RUN echo "  ---> Installing dep" \
    && go get -u github.com/golang/dep/cmd/dep

COPY . .

RUN echo "  ---> Installing dependencies" \
    && dep ensure  \
    && echo "  ---> Run Unit Testing" \
    && go test -timeout 30s  ./... \
    && echo "  ---> Building $APP_VERSION $GOOS-$GOARCH" \
    && go build -a -tags netgo -ldflags '-w -extldflags "-static"' -o bin/server cmd/server/main.go

# final stage
FROM alpine:3.7

COPY --from=build-env /go/src/github.com/fravega/test-go/bin/ /app/

WORKDIR /app

ENV PORT=8080

USER nobody

EXPOSE $PORT

CMD ["/app/server"]