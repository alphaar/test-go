package middlewares

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogger(t *testing.T) {

	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	})

	type args struct {
		next http.HandlerFunc
		name string
	}

	tests := []struct {
		name string
		args args
		want http.HandlerFunc
	}{
		// TODO: Add test cases.
		{name: "Case:Ok", args: args{next: next, name: "test"}, want: next},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Logger(tt.args.next, tt.args.name)
			assert.NotNil(t, got)
		})
	}
}
