package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

type BookResponse struct {
	Title string `json:"title"`
	Page  string `json:"page"`
}

func BookHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		title := params["title"]
		page := params["page"]

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(BookResponse{Title: title, Page: page}); err != nil {
			panic(err)
		}
	})
}
